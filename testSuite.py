import unittest
from myTest import MyFunTest

newSuite = unittest.TestSuite()

newSuite.addTest(unittest.makeSuite(MyFunTest))

if __name__ == '__main__':
    unittest.main()